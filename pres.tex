% Type of the document
\documentclass[shownotes,aspectratio=169]{beamer}

% elementary packages:
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{listings}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{multicol}
\usepackage{biblatex}
\usepackage{csquotes}
\usepackage[font=footnotesize]{caption}
\usepackage{siunitx}

\bibliography{bib}

\setbeamerfont{footnote}{size=\tiny}
\setbeamertemplate{footline}[frame number]

\newcommand\met{E_t^{\text{miss}}}
\newcommand\mtw{m_t^W}
\newcommand\ljet{\text{leading jet}}
\newcommand\lepton{\text{lepton}}
\newcommand\GeV{\giga\electronvolt}
\newcommand\bjet{\text{b-jet}}


% Define the title. Don't forget to insert an abbreviation instead
% of "title for footer". It will appear in the lower left corner:
\title[Low Met]{An overview over variables at low $\met$}
% Define the authors:
\author{Michel Smola} % a-c

\institute{
Humboldt-Universität zu Berlin
}
\titlegraphic{
    \includegraphics[height=2cm]{figures/peony-blossom-bloom-stamen-45864.jpeg}
}
\date{September 14, 2019}

%\hypersetup{pdfpagemode=FullScreen}

%%%%
% Main document
%%%%
\begin{document}
% Draw title page
\frame[plain]{%
\titlepage{}
}

\begin{frame}
    \frametitle{Outline}
    \small
    \tableofcontents
\end{frame}

\section{Explanation}
\begin{frame}
    \frametitle{Fake Efficiency}
    What are we looking for?

    Fake efficiency:
    \begin{align*}
        \epsilon_f &= \frac{\text{\# events Tight}}{\text{\# events Loose}} \\
    \end{align*}
    in a fake enriched region

    $\epsilon_f$ is the efficiency for a fake lepton that passes the loose
    selection to also pass the tight selection.

    The goal is to pick a reasonable, defendable set of candidates for a full efficiency production.
\end{frame}

\begin{frame}
    \frametitle{Criteria}
    \begin{itemize}
        \item Arguably close to SR \\
            That means:
            \begin{itemize}
                \item Single lepton
                \item $>= 1\;\text{b-jets}$?
            \end{itemize}
        \item Smaller than the ones we currently produce
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Fakes}
    What are the actual fake histograms we produce?

    \begin{multicols}{2}
        Our PreSel is:
        \begin{itemize}
            \item $>= 1\;\lepton$ TightLH
            \item second lepton veto MediumLH
            \item $>= 1\;\bjet$
            \item $\met > \SI{120}{\GeV}$
            \item $p_t^{\ljet} > \SI{200}{\GeV}$
        \end{itemize}

        \vfill\null
        \columnbreak

        While for producing the Fake-histograms we run over PreSel-fakes
        \begin{itemize}
            \item $>1 \;\lepton$ MediumLH
            \item second lepton veto MediumLH
            \item $>= 1\;\bjet$
            \item $\met > \SI{120}{\GeV}$
            \item $p_t^{\ljet} > \SI{200}{\GeV}$
        \end{itemize}
    \end{multicols}

    The fakes are produced by weighting data in the PreSel-fakes region
    $w^i = \frac{\epsilon_f}{\epsilon_r-\epsilon_f}(\epsilon_r - \delta_i)$
    which is a small negative number for events with tight leptons (TightLH)
    and a slightly larger positive number for events with
    loose-but-not-tight leptons (MediumLH).
\end{frame}

\begin{frame}
    \frametitle{Fake Region}
    This is the region where we extract our fake efficiencies.
    \begin{multicols}{2}
        We currently use the following fake region:
        \begin{itemize}
            \item Second lepton veto (MediumLH)
            \item $>= 2\;\text{jets}$
            \item $>= 1\;\text{b-jets}$
            \item $\mtw < \SI{20}{\GeV}$
            \item $\met + \mtw < \SI{60}{\GeV}$
        \end{itemize}

        \vfill\null
        \columnbreak

        And the following definitions:
        \vspace{0.4cm}
        \begin{itemize}
            \item Tight Lepton: TightLH
            \item Loose Lepton: MediumLH
        \end{itemize}
    \end{multicols}
\end{frame}

\begin{frame}
    \frametitle{What to tune}
    \begin{itemize}
        \item Find a region with less events with tight leptons
        \item Find a region with more events with loose-but-not-tight leptons
        \item Use LooseLH instead of MediumLH for Loose lepton definition
    \end{itemize}
    Why does LooseLH give us smaller $\epsilon_f$?
    \begin{align*}
        \epsilon_f^{\text{LooseLH}} &= \frac{\text{\# entries TightLH}}{\text{\# entries LooseLH}} \\
        \epsilon_f^{\text{MediumLH}} &= \frac{\text{\# entries TightLH}}{\text{\# entries MediumLH}} \\
        \text{\# entries LooseLH} &> \text{\# entries MediumLH} \\
        \epsilon_f^{\text{LooseLH}} &< \epsilon_f^{\text{MediumLH}}
    \end{align*}
    The downside is that when using LooseLH instead of MediumLH for
    efficiencies we should (probably?) also use LooseLH in the PreSel-fakes
    region which might cancel out the effect and gives us a weird region with
    $>= 1\;\lepton$ at LooseLH but a second lepton veto at MediumLH.
\end{frame}

\section{$\met$, $\mtw$}
\begin{frame}
    \frametitle{$\met$, $\mtw$}
    \begin{multicols}{3}
        \begin{figure}
            \includegraphics[width=\columnwidth]{Plots/All_met_met.pdf}
            \caption{$\met$ for all regions combined}
        \end{figure}
        \begin{figure}
            \includegraphics[width=\columnwidth]{Plots/All_mtw.pdf}
            \caption{$\mtw$ for all regions combined}
        \end{figure}
        \begin{figure}
            \includegraphics[width=\columnwidth]{Plots/All_met_mtw.pdf}
            \caption{$\met + \mtw$ for all regions combined}
        \end{figure}
    \end{multicols}
    Montecarlo is generally underestimated as there are still a few datasets
    missing.
\end{frame}

\begin{frame}
    \frametitle{Efficiencies}
    {\tiny These are efficiencies from data17}
    \begin{figure}
        \begin{subfigure}{0.45\textwidth}
            \includegraphics[width=\columnwidth]{figures/20190828.data17/FakeEfficiency2D_el_ge1j_pt_eta.pdf}
            \caption{$\epsilon_f^{\text{electron}}(p_t^{\text{el}},\eta^{\text{el}})$ for $\mtw < \SI{20}{\GeV}$, $\met + \mtw < \SI{60}{\GeV}$, $>=1\text{jet}$}
        \end{subfigure}
        \begin{subfigure}{0.245\textwidth}
            \includegraphics[width=\columnwidth]{figures/20190828.data17/FakeEfficiency_ge1j_pt.pdf}
            \caption{$\epsilon_f(p_t^{\lepton})$ for $\mtw < \SI{20}{\GeV}$, $\met + \mtw < \SI{60}{\GeV}$, $>=1\text{jet}$}
        \end{subfigure}
        \begin{subfigure}{0.245\textwidth}
            \includegraphics[width=\columnwidth]{figures/20190828.data17/FakeEfficiency_ge1j_eta.pdf}
            \caption{$\epsilon_f(\eta^{\lepton})$ for $\mtw < \SI{20}{\GeV}$, $\met + \mtw < \SI{60}{\GeV}$, $>=1\text{jet}$}
        \end{subfigure}
    \end{figure}
    It is a bit weird that the $\eta$-parametrizations look like that in comparison.
\end{frame}

\section{nJets, nBJets}
\begin{frame}
    \frametitle{nJets, nBJets}
    \begin{multicols}{2}
        \begin{figure}
            \includegraphics[width=0.7\columnwidth]{Plots/All_nJets.pdf}
            \caption{nJets for all regions combined}
        \end{figure}
        \begin{figure}
            \includegraphics[width=0.7\columnwidth]{Plots/All_nBJets.pdf}
            \caption{nBJets for all regions combined}
        \end{figure}
    \end{multicols}
    Cuts on jet/b-jet multiplicity efficiently reduce Q+Jets backgrounds.
\end{frame}

\section{$>= 2\;\text{jets}$, $>= 1\;\text{b-jet}$}
\begin{frame}
    \frametitle{$>= 2\;\text{jets}$, $>= 1\;\text{b-jet}$}
    \begin{multicols}{3}
        \begin{figure}
            \includegraphics[width=\columnwidth]{Plots/ge2j1b_met_met.pdf}
            \caption{$\met$ for $>=2\;\text{jets}$, $>= 1\;\text{b-jet}$}
        \end{figure}
        \begin{figure}
            \includegraphics[width=\columnwidth]{Plots/ge2j1b_mtw.pdf}
            \caption{$\mtw$ for $>=2\;\text{jets}$, $>= 1\;\text{b-jet}$}
        \end{figure}
        \begin{figure}
            \includegraphics[width=\columnwidth]{Plots/ge2j1b_met_mtw.pdf}
            \caption{$\met + \mtw$ for $>=2\;\text{jets}$, $>= 1\;\text{b-jet}$}
        \end{figure}
    \end{multicols}
\end{frame}

\begin{frame}
    \frametitle{Efficiencies}
    {\tiny These are efficiencies from data17}
    \begin{figure}
        \begin{subfigure}{0.45\textwidth}
            \includegraphics[width=\columnwidth]{figures/20190828.data17/FakeEfficiency2D_el_ge2j1b_pt_eta.pdf}
            \caption{$\epsilon_f^{\text{electron}}(p_t^{\text{el}},\eta^{\text{el}})$ for $\mtw < \SI{20}{\GeV}$, $\met + \mtw < \SI{60}{\GeV}$, $>=2\;\text{jets}$, $>=1\;\text{b-jet}$ from data17}
        \end{subfigure}
        \begin{subfigure}{0.245\textwidth}
            \includegraphics[width=\columnwidth]{figures/20190828.data17/FakeEfficiency_ge2j1b_pt.pdf}
            \caption{$\epsilon_f(p_t^{\lepton})$ for $\mtw < \SI{20}{\GeV}$, $\met + \mtw < \SI{60}{\GeV}$, $>=2\;\text{jets}$, $>=1\;\text{b-jet}$ from data17}
        \end{subfigure}
        \begin{subfigure}{0.245\textwidth}
            \includegraphics[width=\columnwidth]{figures/20190828.data17/FakeEfficiency_ge2j1b_eta.pdf}
            \caption{$\epsilon_f(\eta^{\lepton})$ for $\mtw < \SI{20}{\GeV}$, $\met + \mtw < \SI{60}{\GeV}$, $>=2\;\text{jets}$, $>=1\;\text{b-jet}$}
        \end{subfigure}
    \end{figure}
    The b-jet cut brings a slight improvement.
\end{frame}

\section{Leading Jet}
\begin{frame}
    \frametitle{Leading Jet}
    \begin{multicols}{2}
        \begin{figure}
            \includegraphics[width=0.8\columnwidth]{Plots/All_jet_pt.pdf}
            \caption{$p_t^{\ljet}$ for all regions combined}
        \end{figure}
        \begin{figure}
            \includegraphics[width=0.8\columnwidth]{Plots/All_jet_eta.pdf}
            \caption{$\eta^{\ljet}$ for all regions combined}
        \end{figure}
    \end{multicols}
\end{frame}

\begin{frame}
    \frametitle{Efficiencies}
    {\tiny These are efficiencies from data15 and 16}
    \begin{figure}
        \begin{subfigure}{0.45\textwidth}
            \includegraphics[width=0.9\columnwidth]{figures/20190822.data1516/FakeEfficiency2D_el_ge2j1b_pt_eta.pdf}
            \caption{$\geq 2\;\text{jets}$, $\geq 1\;\bjet$}
        \end{subfigure}
        \begin{subfigure}{0.45\textwidth}
            \includegraphics[width=0.9\columnwidth]{figures/20190822.data1516/FakeEfficiency2D_el_jetpt200_ge2j1b_pt_eta.pdf}
            \caption{$\geq 2\;\text{jets}$, $\geq 1\;\bjet$, $p_t^{\ljet} > \SI{200}{\GeV}$}
        \end{subfigure}
    \end{figure}
    The difference is insignificant so we should not make this cut to preserve statistics.
\end{frame}

\section{Lepton}
\begin{frame}
    \frametitle{Lepton}
    \begin{multicols}{3}
        \begin{figure}
            \includegraphics[width=0.9\columnwidth]{Plots/All_lepton_pt.pdf}
            \caption{$p_t^{\lepton}$ for all regions combined}
        \end{figure}
        \begin{figure}
            \includegraphics[width=0.9\columnwidth]{Plots/All_lepton_eta.pdf}
            \caption{$\eta^{\lepton}$ for all regions combined}
        \end{figure}
        \begin{figure}
            \includegraphics[width=0.9\columnwidth]{Plots/All_dphi_met_lepton.pdf}
            \caption{$\Delta \phi (\met, \lepton)$ for all regions combined}
        \end{figure}
    \end{multicols}
    $\Delta \phi$ might be a candidate to improve the fake region.
\end{frame}

\section{Suggestions}
\begin{frame}
    \frametitle{Suggestions}
    \begin{itemize}
        \item Use a tighter selection for the fake-enriched region, e.g.
            $\met+\mtw < \SI{50}{\GeV}, \SI{40}{\GeV}$ or maybe add a cut on
            $\Delta \phi$?
    \end{itemize}
\end{frame}

\end{document}
