TARGETS = pres.pdf
SOURCES = $(shell find . -name '*.tex')
#PNGS = $(shell ls img/*.png | sed -e 's/.png$$/.pdf/g')

pres.pdf: pres.tex $(SOURCES)
	latexmk -pdf -use-make pres.tex

img/%.pdf: img/%.png
	convert $< $@

clean:
	latexmk -CA
	rm -rf Tables Plots
	rm figures/20190828.data17/* figures/20190822.data1516/*

open:
	evince $(TARGETS) &

pull:
	rsync -rv smolamic@warp.zeuthen.desy.de:lustre/Code/TRExFitter/VLQWbConfigs/Trex_DL1_85/{Tables,Plots} .
	rsync -v smolamic@warp.zeuthen.desy.de:lustre/Code/TTHbbAnalysis/TTHbbAnalysis/VLQWbLepOffline/share/efficiency/20190828.data17/{FakeEfficiency2D_el_ge1j_pt_eta.pdf,FakeEfficiency2D_el_ge2j1b_pt_eta.pdf,FakeEfficiency_ge1j_pt.pdf,FakeEfficiency_ge1j_eta.pdf,FakeEfficiency_ge2j1b_pt.pdf,FakeEfficiency_ge2j1b_eta.pdf} figures/20190828.data17/
	rsync -v smolamic@warp.zeuthen.desy.de:lustre/Code/TTHbbAnalysis/TTHbbAnalysis/VLQWbLepOffline/share/efficiency/20190828.data17/{FakeEfficiency2D_el_ge2j1b_pt_eta.pdf,FakeEfficiency2D_el_jetpt200_ge2j1b_pt_eta.pdf} figures/20190822.data1516/
